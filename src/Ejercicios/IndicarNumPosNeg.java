/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class IndicarNumPosNeg {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("INDICAR NUMERO POSITIVO O NEGATIVO");
        int num;
        System.out.print("Introduzca un número: ");
        num = scan.nextInt();
        
        while(num!=0) 
        {
            if(num>0)
            {
                System.out.println("Positivo");
            }
            else
            {
                System.out.println("Negativo");
            }
            
            System.out.print("Introduzca otro número: ");
            num = scan.nextInt();
        } 
    }
    
}
