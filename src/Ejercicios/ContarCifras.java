/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class ContarCifras {
    public static void main(String[] args) {
    
        Scanner entrada = new Scanner(System.in);
        String num;
        int cifras = 0;
        
        System.out.println("CONTAR CIFRAS DE UN NUMERO\n");
        
        System.out.print("Ingrese una cifra entre 0 y 9.999 : \n>");
        
        num = entrada.nextLine();//obtener la data
        
        //validar
        if(Double.parseDouble(num ) >= 0 && Double.parseDouble(num )<= 9.999){//comprobar que este dentro del rango
            
            if(num.length() > 1)
                cifras = num.length() -1;
            else cifras = num.length();
            
            System.out.println("El numero tiene "+cifras+" cifras");
            
        }else System.err.println("El numero esta fuera del rango");
        
        
        
        
        
    
    }
}
