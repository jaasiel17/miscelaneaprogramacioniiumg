/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class NumMultiplos {
    
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        short numUno, numDos;
        
        System.out.println("NUMERO MULTIPLO DE OTRO\n");
        
        System.out.print("Ingrese el valor uno > "); numUno = entrada.nextShort();
        System.out.print("Ingrese el valor dos > "); numDos = entrada.nextShort();
        
        if( numUno %numDos == 0 )
            System.out.println("El numero "+numUno+" es multiplo de " +numDos);
        else
            System.out.println("El numero "+ numUno+ " no es multiplo de "+numDos);
        //System.out.println(numUno %numDos);
        
        
    }
    
}
