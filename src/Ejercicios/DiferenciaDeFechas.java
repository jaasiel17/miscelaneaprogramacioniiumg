/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class DiferenciaDeFechas {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int dia, mes, anio, dia2, mes2, anio2, difDia, difMes, difAnio;
        
        System.out.print("Fecha 1: Ingrese el dia -> "); dia = entrada.nextInt();
        System.out.print("Fecha 1: Ingrese el mes -> "); mes = entrada.nextInt();
        System.out.print("Fecha 1: Ingrese el anio -> "); anio = entrada.nextInt();
        
        System.out.print("Fecha 2: Ingrese el dia -> "); dia2 = entrada.nextInt();
        System.out.print("Fecha 2: Ingrese el mes -> "); mes2 = entrada.nextInt();
        System.out.print("Fecha 2: Ingrese el anio -> "); anio2 = entrada.nextInt();
        
        if(mes > 12 || mes2 > 12) System.out.println("Mes incorrecto");
        if(dia > 30 || dia2 > 30) System.out.println("Dia incorrecto");
        else{
            difDia = Math.abs( dia - dia2);
            difMes = Math.abs(mes - mes2);
            difAnio = Math.abs(anio - anio2);
        
            System.out.println("Hay " + difDia + " dias " + difMes + " meses " + difAnio+ " anios de diferencia entre las fechas");
        }
        
        
    }
    
}
