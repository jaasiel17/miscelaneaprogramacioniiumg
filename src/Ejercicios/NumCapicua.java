/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class NumCapicua {
    
    public static void main(String[] args) {
        int num;
        int dm, um, c, d, u;
        Scanner scan = new Scanner(System.in);
        boolean capicua = false; 
        
        System.out.println("NUMERO CAPICUA\n");
        
        System.out.print("Introduzca un número entre 0 y 99.999: ");
        num = scan.nextInt();
        // unidad
        u = num % 10;
        num = num / 10;
        // decenas
        d = num % 10;
        num = num / 10;
        // centenas
        c = num % 10;
        num = num / 10;
        // unidades de millar
        um = num % 10;
        num = num / 10;
        // decenas de millar
        dm = num;
        //si el número tiene 5 cifras (dm, um, c, d, u)
        if (dm == u && um == d)
        {
            capicua = true;
        }
        //si el número tiene 4 cifras (0, um, c, d, u)
        if (dm == 0 && um == u && c == d)
        {
            capicua = true;
        }
        //si el número tiene 3 cifras (0, 0, c, d, u)
        if (dm == 0 && um==0 && c == u)
        {
            capicua = true;
        }
        //si el número tiene 2 cifras (0, 0, 0, d, u)
        if (dm == 0 && um == 0 && c == 0 && d == u)
        {
            capicua = true;
        }
        // se entiende que un número de una cifra no es capicúa
        if (capicua)
        {
        System.out.println ("el número es capicúa");
        }
        else
        {
         System.out.println ("el número NO es capicúa");
        }
    }
    
}
