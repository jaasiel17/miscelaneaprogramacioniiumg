/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class FechaCorrecta {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int anio, mes, dia;
        
        System.out.println("VERIFICAR FECHA\n");
        
        System.out.print("Ingrese el dia: "); dia = entrada.nextInt();
        System.out.print("Ingrese el mes: "); mes = entrada.nextInt();
        System.out.print("Ingrese el anio: "); anio = entrada.nextInt();
        
        
        
        if( mes >12 ) System.out.println("La fecha es incorrecta, verifique");
        else if(dia > 30 ) System.out.println("La fecha es incorrecta, verifique");
        else{
            System.out.println("La fecha es correcta: " +  dia +"/"+mes+"/"+anio);
        }
        
         
    }
    
}
