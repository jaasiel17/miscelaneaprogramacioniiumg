/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class SumarNumIntroducidos {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("SUMAR NUMEROS INTRODUCIDOS");
        
        int num, suma;
        suma = 0;
        
        do
        {
            System.out.print("Introduzca un número: ");
            num = scan.nextInt();
            suma += num;
        }
        while(num!=0);

        System.out.println("La suma de todos los números es: "+suma);
    }
    
}
