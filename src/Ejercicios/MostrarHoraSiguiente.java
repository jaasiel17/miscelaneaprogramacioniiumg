/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class MostrarHoraSiguiente {
    
    public static void main(String[] args) {
        
        System.out.println("MOSTRAR LA HORA EN EL SEGUNDO SIGUIENTE\n");
        
        Scanner scan = new Scanner(System.in);
        
        int h,m,s; // hora, minutos y segundos
        System.out.print("Introduzca hora: ");
        h = scan.nextInt();
        System.out.print("Introduzca minutos: ");
        m = scan.nextInt();
        System.out.print("Introduzca segundos: ");
        s = scan.nextInt();
        // suponemos que la hora introducida es correcta

        // incrementamos los segundos
        s ++;
        // si los segundos superan 59, los reiniciamos a 0 e incrementamos los minutos
        if (s >= 60)
        {
        s = 0;
        m ++;
        // si los minutos superan 59, los reiniciamos a 0 e incrementamos la hora
        if (m >= 60)
        {
        m = 0;
        h ++;
        // si la hora supera 23, la reiniciamos a 0
        if (h>=24)
        h= 0;
        }
        }
        System.out.println ("Fecha: "+ h + ":"+ m + ":" + s);
        
    }
    
}
