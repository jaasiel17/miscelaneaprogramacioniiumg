/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class MostrarNotaTexto {
    
    private static void consola(String txt){
        System.out.println(txt);
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("MOSTRA NOTA EN LETRAS\n");
        
        int num;
        System.out.print("Introduzca una nota numérica entre 0 y 10: ");
        num = scan.nextInt();
        
        switch(num){
            case 0:
                consola("Cero");
            break;
            case 1:
                consola("Uno");
            break;
            case 2:
                consola("Dos");
            break;
            case 3:
                consola("Tres");
            break;
            case 4:
                consola("Cuatro");
            break;
            case 5:
                consola("Cinco");
            break;
            case 6:
                consola("Seis");
            break;
            case 7:
                consola("Siete");
            break;
            case 8:
                consola("Ocho");
            break;
            case 9:
                consola("Nueve");
            break;
            case 10:
                consola("Diez");
            break;
        }
    }
    
}
