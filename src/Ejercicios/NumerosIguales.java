/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class NumerosIguales {
    public static void main(String [] args)
    {
        Scanner entradaTeclado = new Scanner(System.in);
        double numUno, numDos;
        
        System.out.println("NUMEROS IGUALES \n");
        System.out.print("Ingrese un numero > "); numUno = entradaTeclado.nextDouble();
        System.out.print("Ingrese otro numero > "); numDos = entradaTeclado.nextDouble();
        
        if(numUno == numDos)
            System.out.println("Los numeros ingresados son iguales");
        else
            System.out.println("los numeros ingresados son diferentes");
    }
}
