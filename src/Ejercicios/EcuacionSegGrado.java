/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class EcuacionSegGrado {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double a = 0.00, b = 0.00, c = 0.00, aux = 0.00 , x1 = 0.00, x2 = 0.00, x = 0.00;
        
        System.out.println("ECUACION DE SEGUNDO GRADO\n");
        
        System.out.print("Ingrese el valor a: ");  a = entrada.nextDouble();
        System.out.println("Ingrese el valor de b: "); b = entrada.nextDouble();
        System.out.println("Ingrese el valor de c: "); c = entrada.nextDouble();
        
        aux = Math.pow(b, 2) - 4 * a * c;
        
        if(aux < 0 ) System.out.println("La ecuacion no tiene solucion");
        else if(aux == 0){
            x = - b /(2 * a);   
            System.out.println("La ecuacion tiene solo una solucion, X = "+ x);
        }else{
            x1 = (- b + ( Math.sqrt(Math.pow(b, 2)  - 4 * a * c) ) ) / (2 * a);
            x2 = (- b - ( Math.sqrt(Math.pow(b, 2) - 4 * a * c)  ) ) / (2 * a);
            System.out.println("Primera solucion, X1 = " + x1);
            System.out.println("Segunda solucion, X2 = " + x2);
        }
        
        
    }
    
}
