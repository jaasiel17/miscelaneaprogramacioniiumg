/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class FechaSiguiente {
    
    public static void main(String[] args) {
        
        System.out.println("MOSTRAR FECHA SIGUIENTE\n");
        
        Scanner scan = new Scanner(System.in);
        
        int dia,mes,año;
        System.out.print("Introduzca día: ");
        dia = scan.nextInt();
        System.out.print("Introduzca mes: ");
        mes = scan.nextInt();
        System.out.print("Introduzca año: ");
        año = scan.nextInt();
       
        dia ++;
        
        if (dia >= 30)
        {
            dia = 1;
            mes ++;
           
            if (mes >= 12)
            {
                mes = 1;
                año ++;
            }
        }
       
        if (año == 0)
        {
            año = 1;
            System.out.println (dia + "/"+ mes + "/" + año);
        }
    }
    
}
