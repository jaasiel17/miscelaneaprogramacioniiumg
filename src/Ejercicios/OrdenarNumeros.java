/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class OrdenarNumeros {
    public static void main(String[] args) {
        
        float numUno, numDos, numTres, auxiliar;
        Scanner entrada = new Scanner(System.in);
        int opcion;
        
        System.out.println("ORDENAR NUMEROS\n");
        
        System.out.println("1- Dos numeros\n2- Tres numeros");
        System.out.print("> "); opcion = entrada.nextInt();
        
        if(opcion == 1){
            System.out.print("Ingrese el primer valor > "); numUno = entrada.nextFloat();
            System.out.print("Ingrese el segundo valor > "); numDos = entrada.nextFloat();
            
            if (numUno > numDos) System.out.println("De mayor a menor: " +numUno+ " , " + numDos);
            else System.out.println("De mayor a menor: " +numDos+ " , " + numUno);
                    
        }else if(opcion == 2){
            System.out.print("Ingrese el primer valor > "); numUno = entrada.nextFloat();
            System.out.print("Ingrese el segundo valor > "); numDos = entrada.nextFloat();
            System.out.print("Ingrese el tercer valor > "); numTres = entrada.nextFloat();
            
            for (int i = 0; i < 3; i++) {
                if(numUno < numDos){
                    auxiliar = numUno;
                    numUno = numDos;
                    numDos = auxiliar;
                }
                if(numDos < numTres){
                    auxiliar = numDos;
                    numDos = numTres;
                    numTres = auxiliar;
                }
            }
            
            
            
            System.out.println("De mayor a menor: "+ numUno+" , "+numDos+" , "+numTres);
            
            
            
        }
        
    }
}
