/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import static java.lang.Math.*;
import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Circulo {
    

    
    public static void main(String [] args)
    {
        
        Scanner entradaTeclado = new Scanner(System.in);
        double Radio = 0;
        int opcion = 0;
        
        System.out.println("OPERACIONES CON CIRCULOS \n \n");
        System.out.print("1- CALCULAR AREA \n2- CALCULAR LONGITUD");
        System.out.print("\n> ");
        
        opcion = entradaTeclado.nextInt();
        
        switch(opcion)
        {
            case 1:
                System.out.print("Ingrese el radio > ");
                Radio = entradaTeclado.nextDouble();
                System.out.println("El Area es: " + PI * pow(Radio,2));
                break;
            case 2:
                System.out.print("Ingrese el radio > ");
                Radio = entradaTeclado.nextDouble();
                System.out.println("La longitud es: " + (2*Radio) * PI);
                break;
                
            default:
                System.out.println("Opcion no valida");
                break;
        }
        
    }
    
    
    
}
