/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class CantNumIngresados {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int numIngresado = 0, contador = 0;
        ArrayList<Integer> arreglo = new ArrayList<Integer>();
        
        System.out.println("MOSTRAR Y CONTAR NUMEROS INTRODUCIDOS\n");

        System.out.println("Introducir alor: ");
        numIngresado = entrada.nextInt();

        while(numIngresado >= 0)
        {   
            if(numIngresado >= 0) arreglo.add(numIngresado);

            System.out.println("Introducir Valor: ");
            numIngresado = entrada.nextInt();
            contador++;
        }

        //mostrar
        System.out.println("Ingreso "+contador+" numeros\nValores Ingresados:");
        for (int i = 0; i < arreglo.size(); i++) {
            
            System.out.println("Pos : " + i+1 +" Valor: " + arreglo.get(i));
        }
    }
    
}
