/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class CifrasAlReves {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String num =  "", a = "", b = "";
                
        System.out.println("CIFRAS INVERSAS\n");
        
        System.out.print("Ingrese una cifra entre 0 y 9.999 : \n>");
        
        num = entrada.nextLine();//obtener la data
        
        //validar
        if(Double.parseDouble(num ) >= 0 && Double.parseDouble(num )<= 9.999){//comprobar que este dentro del rango
            
            if(num.length() > 2) a = num.substring(2,num.length());//comprobar si tiene cifras despues del punto
            
            if(num.length() > 1) b = "." + num.substring(0,1);
            else b = num.substring(0,1);
            
            num = "";//limpiar
            
            num = a + b;//aniadir cifras 
            
            System.out.println("Numer al reves : "+ num);
            
        }else System.err.println("El numero esta fuera del rango");
        
        
        
    }
    
}
