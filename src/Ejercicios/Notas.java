/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Notas {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        short nota = 0;
        
        System.out.println("NOTAS DE ALUMNO\n");
        
        System.out.print("Ingrese su nota [entre 0 y 10]\n> ");
        
        nota = entrada.nextShort();//obtener la nota por teclado
        
        if(nota > 10 || nota < 0)
            System.err.print("La nota esta fuera del rango");
        else{
            
            if( nota >= 0 && nota <5)
            {
                System.out.println("Insuficiente :'(");
            }else if(nota >= 5 && nota < 8)
            {
                System.out.println("Suficiente :|");                
            }else if(nota >= 8 && nota < 10){
                System.out.println("Bien !!  :)");
            }else{
                System.out.println("Excelente!!! :) :)");
            }
            
        }
    
    }
    
    
}
