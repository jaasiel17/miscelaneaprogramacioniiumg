/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AdivinarNumero {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        System.out.println("ADIVINAR NUMERO\n");
        
        int n, num;
        n=(int)(Math.random()*100)+1;
       

        System.out.print("Introduce número: ");
        num = scan.nextInt();

        while(num!=n) 
        {
            if(num>n)
            {
                System.out.println("menor");
            }
            else
            {
            System.out.println("mayor");
            }
            
            System.out.print("Introduce número: ");
            num = scan.nextInt();
        }

        System.out.println("acertaste..."); 

        
    }
    
}
