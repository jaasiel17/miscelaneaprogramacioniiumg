/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class NumMayorIgual {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        float numUno, numDos;
        
        System.out.println("EL NUMERO MAYOR O IGUAL\n");
              
        System.out.print("Ingrese el valor uno: "); numUno = entrada.nextFloat();
        System.out.print("Ingrese el valor dos: "); numDos = entrada.nextFloat();
                
        if(numUno > numDos) System.out.println("El numero " +numUno+" es el mayor");
        else if(numDos > numUno)System.out.println("El numero "+numDos+" es el mayor");   
        else System.out.println("Los numeros son iguales");
                
    }
    
}
