/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class IndicarParImpar {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("INDICAR NUMERO PAR O IMPAR");
        
        int num;
        System.out.print("Introduzca un número: ");
        num = scan.nextInt();
        
        while(num!=0) 
        {
            if(num%2 == 0)
            {
                System.out.println("Par");
            }
            else
            {
                System.out.println("Impar");
            }
            
        System.out.print("Introduzca otro número: ");
        num = scan.nextInt();
        }   
    }
    
}
